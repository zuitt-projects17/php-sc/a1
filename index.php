<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Ativity1</title>
</head>
<body>

	<h1>Full Address</h1>
	<p><?php echo getFullAddress("3F Caswynn Bldg Timog Avenue", "Quezon City", "Metro Manila", "Philipines"); ?></p>
	<p><?php echo getFullAddress("3F Enzo Bldg Buendia Avenue", "Makati City", "Metro Manila", "Philipines"); ?></p>


	<h1>Letter-Based Grading</h1>
	<p><?php echo getLetterGrade(87); ?></p>
	<p><?php echo getLetterGrade(94); ?></p>
	<p><?php echo getLetterGrade(74); ?></p>

</body>
</html>